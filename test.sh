#!/bin/sh -e
#
#    test.sh: check openssl-vulnkey script
#    Copyright (C) 2008 Canonical Ltd.
#    Author: Jamie Strandboge <jamie@canonical.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 2,
#    as published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

good_mod="AE464CB1F29E069310788880C0154F32B40C71B6BBC07B37E5B323B9071217A2B1F345022AE86CF5329CD8EBAFD7E046EDDB06BB689397115222C646C449872ADBFAFD13021BD6B5A63AB38B4016B3AA8625E0F34488925F8632183C1E597E49FA9A21A6479CBCCE5DB2FB435EEA4236595CAEDFB07AFCB0E7A83B826D8B835C732282FB795ABADF05DE88607AC94ED25545B2E07D000F0AC42ACA9FE7023AAD0885F5CD1C8CCE99621FBC5885A115B9F8881AB4D75F657858E1C566C65FF3853365F831E603FD94448A62717B4A051323CB52A401AA7B54D25289A2ABAF9092B1C293DEF53D4FDF4C848426335522688D77F9C099C8202275AB4A2B79379229"
good_files="examples/good_req.csr examples/good_x509.pem examples/good_rsa.key"
bad_mod="BDDF1E2F255A193DF3FE272DD9F63CC24975D6FC33F785912B76460ED99735CAFA939EBEB8FB06EBCFD6B3923E9C953F360BCA604EE181CD83930F20FEC7087D4E500897CF218FDF96EB33F46455105D77CD0A43AC80559A92A83DD8218634F7649FD02DDB045E0D57D00F7116E354B73091A762292BEC7483B47E07BC31FF01"
bad_files="examples/bad_req.csr examples/bad_x509.pem examples/bad_rsa.key examples/bad_rsa_4096.pem examples/bad_x509_4096.pem"
error=

tmpdir=`mktemp -d`
trap "rm -rf $tmpdir" EXIT HUP INT QUIT TERM
chmod a+x ./openssl-vulnkey

# setup files
cp -a ./openssl-vulnkey ./examples $tmpdir
for b in 512 1024 2048 4096
do
    cat blacklists/*/*${b}* | cut -d ' ' -f 5 | cut -b21- | sort >> $tmpdir/blacklist.RSA-${b}
done
cd $tmpdir
sed -i "s#^db_prefix .*#db_prefix = '$tmpdir/blacklist.RSA-'#"  $tmpdir/openssl-vulnkey

# bad args
echo -n "no args: "
if ./openssl-vulnkey >/dev/null ; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

echo -n "no modulus: "
if ./openssl-vulnkey -b 1024 >/dev/null ; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

echo -n "no bits: "
if ./openssl-vulnkey -m $bad_mod >/dev/null ; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

# expect bad
for i in $bad_files
do
    f=`basename $i`
    echo ""
    echo "$f: "
    if ./openssl-vulnkey $i ; then
        echo "FAIL"
        error="yes"
    else
        echo "PASS"
    fi

    echo ""
    echo "$f (stdin): "
    if cat $i | ./openssl-vulnkey - ; then
        echo "FAIL"
        error="yes"
    else
        echo "PASS"
    fi
done

echo ""
echo "all bad files ($bad_files): "
if ./openssl-vulnkey $bad_files ; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

echo ""
echo "bad modulus: "
if ./openssl-vulnkey -b 1024 -m $bad_mod ; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

# expect good
for i in $good_files
do
    f=`basename $i`
    echo ""
    echo "$f: "
    if ./openssl-vulnkey $i ; then
        echo "PASS"
    else
        echo "FAIL"
        error="yes"
    fi

    echo ""
    echo "$f (stdin): "
    if cat $i | ./openssl-vulnkey - ; then
        echo "PASS"
    else
        echo "FAIL"
        error="yes"
    fi
done

echo ""
echo "all good files ($good_files): "
if ./openssl-vulnkey $good_files ; then
    echo "PASS"
else
    echo "FAIL"
    error="yes"
fi

echo ""
echo "some bad files, some good files ($bad_files $good_files): "
if ./openssl-vulnkey $bad_files $good_files ; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

echo ""
echo "good modulus: "
if ./openssl-vulnkey -b 2048 -m $good_mod ; then
    echo "PASS"
else
    echo "FAIL"
    error="yes"
fi

echo ""
echo "Non-existent file:"
if ./openssl-vulnkey ./nonexistent 2>/dev/null || [ "$?" != "2" ]; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

echo ""
echo "Permission denied:"
noperms="$tmpdir/unreadable"
touch "$noperms"
chmod 0 "$noperms"
if ./openssl-vulnkey "$noperms" 2>/dev/null || [ "$?" != "2" ]; then
    echo "FAIL"
    error="yes"
else
    echo "PASS"
fi

# cleanup and report
cd - >/dev/null

echo ""
echo "----------------------"
if [ "$error" = "yes" ]; then
    echo "FAILED"
    exit 1
else
    echo "PASSED"
fi

exit 0
